package com.example.myapplication;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private TextView weatherTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        weatherTextView = findViewById(R.id.ShowWeather);

        OkHttpClient client = new OkHttpClient();

        // Ubicacion por medio de codigo postal en este caso us.33109
        // Puedes cambiar investigando en https://developer.weatherunlocked.com/documentation/localweather/current

        Request request = new Request.Builder()
                .url("http://api.weatherunlocked.com/api/forecast/us.33109?app_id=8a917cb6&app_key=8095971487b61f9218eb5c31263d4753")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    final String responseData = response.body().string();

                    Gson gson = new Gson();
                    JsonObject jsonObject = gson.fromJson(responseData, JsonObject.class);

                    JsonArray daysArray = jsonObject.getAsJsonArray("Days");
                    JsonObject firstDayObject = daysArray.get(0).getAsJsonObject();
                    JsonArray timeframesArray = firstDayObject.getAsJsonArray("Timeframes");
                    JsonObject firstTimeframeObject = timeframesArray.get(0).getAsJsonObject();
                    double tempC = firstTimeframeObject.get("temp_c").getAsDouble();

                    final String tempCValue = String.valueOf(tempC);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            weatherTextView.setText(tempCValue);
                        }
                    });
                }
            }
        });
    }
}